const path = require('path')
const webpack = require('webpack')
const CopyPlugin = require('copy-webpack-plugin')

// Import app configuration
const appConfig = require('./src/app.config.json')
// JS output subdirectory
const jsOutputPath = 'js'

// Configuration
module.exports = {
  // Full url for development, else default
  publicPath: process.env.NODE_ENV === 'development' ? 'http://localhost:8081/' : '/',
  // Disable asset filenames hashing
  filenameHashing: false,
  // Overwrite webpack-dev-server config
  devServer: {
    // Allow all origin to bypass CORS in development mode
    headers: {
      'Access-Control-Allow-Origin': '*'
    }
  },
  // Webpack configuration
  configureWebpack: {
    // Entry file
    entry: {
      app: path.resolve(__dirname, 'src/app.js'),
      update: path.resolve(__dirname, 'src/update.js')
    },
    // Output
    output: {
      filename: `${jsOutputPath}/[name].js`
    },
    // Aliases
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src'),
        '#styles': path.resolve(__dirname, 'src/scss')
      }
    },
    // Externals
    externals: {
      vue: 'lib.Vue' // target window.lib.Vue
    },
    // Code splitting
    optimization: {
      splitChunks: {
        cacheGroups: {
          // Vendors
          commons: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendors',
            filename: `${jsOutputPath}/[name].js`,
            chunks: 'all'
          }
        }
      }
    },
    // Webpack plugins
    plugins: [
      // Copy app.config.json file
      new CopyPlugin([
        { from: './src/app.config.json', to: 'app.config.json' }
      ]),
      // Define 'constants'
      new webpack.DefinePlugin({
        // Shortcut for Vue
        __VUE__: 'window.lib.Vue',
        // Shortcut for MyStation
        __MYSTATION__: 'window.__MYSTATION_VUE_INSTANCE__',
        // Shortcut to MyStation API Manager instance
        __MYSTATION_API__: 'window.__MYSTATION_VUE_INSTANCE__.$APIManager.use(\'myStationAPI\')',
        // Replaced by the app name to help the app developer
        __APP_NAME__: `'${appConfig.name}'`,
        // App version
        __APP_VERSION__: `'${appConfig.version}'`,
        // App color
        __APP_COLOR__: `'${appConfig.color}'`,
        // Translations prefix
        __T_PREFIX__: `'apps.${appConfig.name}'`
      })
    ]
  },
  css: {
    loaderOptions: {
      scss: {
        // To have access to variables in all CHILD SASS files
        prependData: `
          $__APP_NAME__: "${appConfig.name}";
          @import "~#styles/_variables.scss";
        `
      }
    }
  }
}
