// Main app component
import App from '@/App.vue'
// Translations
import i18n from '@/i18n'
// Pancakes
import MyFeedsPancake from '@/components/pancakes/MyFeedsPancake'
// Store modules
import MyFeedsModule from '@/store/MyFeedsModule'
// Styles
import '#styles/main.scss'

// Define app object
const app = {
  // Translations
  i18n: i18n,
  // Components
  components: {
    // Main app component
    app: App,
    // Pancakes (MyStation Dashboard)
    pancakes: {
      MyFeedsPancake: MyFeedsPancake
    }
  },
  // Shared data
  store: {
    modules: {
      MyFeedsModule: MyFeedsModule
    }
  },
  // App style files to import
  styles: {
    app: 'app.css'
  }
}

// You can access to MyStation everywhere using '__MYSTATION__'
__MYSTATION__.registerApp(__APP_NAME__, app)
