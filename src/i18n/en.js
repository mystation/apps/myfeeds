export default {
  global: {
    category: 'category',
    categories: 'categories',
    feed: 'feed',
    feeds: 'feeds',
    edit: 'edit',
    newCategory: 'new category',
    addSource: 'add a source',
    label: 'label',
    all: 'all',
    lastArticles: 'last articles',
    createdCategories: 'created categories',
    noCategoryMessage: 'No category created.{0}Create a category to add feeds.',
    noFeedsMessage: 'No source found.'
  },
  tabs: {
    feeds: 'feeds',
    options: 'options'
  },
  modals: {
    addSource: {
      title: 'add a source',
      okTitle: 'add',
      checkUrlMessage: 'Check url...',
      notFoundMessage: 'No feed found... :(',
      pleaseEnterUrlMessage: 'Please enter url'
    },
    editSource: {
      title: 'edit source',
      okTitle: 'save',
      deleteSource: 'delete source',
      confirmDeleteMessage: 'Delete source?'
    },
    createCategory: {
      title: 'create a category',
      okTitle: 'create'
    },
    editCategory: {
      title: 'edit category',
      okTitle: 'save',
      deleteCategory: 'delete category',
      confirmDeleteMessage: 'Delete category?{0}All related sources (feeds) will be removed too'
    }
  }
}
