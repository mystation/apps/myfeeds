export default {
  global: {
    category: 'catégorie',
    categories: 'catégories',
    feed: 'feed',
    feeds: 'feeds',
    edit: 'éditer',
    newCategory: 'nouvelle catégorie',
    addSource: 'ajouter une source',
    label: 'label',
    all: 'tout',
    lastArticles: 'derniers articles',
    createdCategories: 'catégories créées',
    noCategoryMessage: 'Aucune catégorie créée.{0}Créer une catégorie pour ajouter des sources (feeds).',
    noFeedsMessage: 'Aucune source.'
  },
  tabs: {
    feeds: 'feeds',
    options: 'options'
  },
  modals: {
    addSource: {
      title: 'ajouter une source',
      okTitle: 'ajouter',
      checkUrlMessage: 'Vérification de l\'adresse...',
      notFoundMessage: 'Aucun feed trouvé à cette adresse... :(',
      pleaseEnterUrlMessage: 'Veuillez saisir une adresse'
    },
    editSource: {
      title: 'modifier la source',
      okTitle: 'enregistrer',
      deleteSource: 'supprimer la source'
    },
    deleteSource: {
      confirmDeleteMessage: 'Supprimer la source?'
    },
    listCategorySources: {
      title: 'sources de la catégorie'
    },
    createCategory: {
      title: 'créer une catégorie',
      okTitle: 'créer'
    },
    editCategory: {
      title: 'modifier la catégorie',
      okTitle: 'enregistrer',
      deleteCategory: 'supprimer la catégorie'
    },
    deleteCategory: {
      confirmDeleteMessage: 'Supprimer la catégorie?{0}Toutes les sources liées seront également supprimées'
    }
  }
}
