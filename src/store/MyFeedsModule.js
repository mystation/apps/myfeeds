// Get easy access to server API
// use directly global: __MYSTATION_API__
// OR :
// const ServerAPI = __MYSTATION__.$APIManager.use('myStationAPI')

// RSS Parser
import Parser from 'rss-parser'
const CORS_PROXY = 'https://cors-anywhere.herokuapp.com/'

// Lodash
const _ = window.lib._

// MyStation classes
const API = __MYSTATION__.class.API
const APIOptions = __MYSTATION__.class.APIOptions

// API options
const apiOptions = new APIOptions(
  // Name
  __APP_NAME__ + '-baseFeedAPI',
  // Driver
  'axios',
  // Options
  {
    // baseURL: '',
    allowedMethods: ['GET'],
    headers: {
      // 'Access-Control-Allow-Origin': '*',
      // 'X-Requested-With': 'XMLHttpRequest'
    }
  }
)

// Define a base API to retrieve feed (& articles) meta
const api = new API(apiOptions)
const baseFeedAPI = __MYSTATION__.$APIManager.add(api)

/**
 * Retrieve 'enclosure' or the Open Graph defined img for article
 * @param {Object} item - The item object
 * @return {Object} An object with url, secure_url, width and height properties
 */
async function findArticleImg (item) {
  // Firstly check for 'enclosure' definition
  if (item.enclosure && item.enclosure.url) {
    return {
      url: item.enclosure.url,
      secure_url: item.enclosure.url
    }
  }
  // Else, get Open Graph meta tag on page
  let response, htmlContent
  try {
    response = await baseFeedAPI.request('GET', CORS_PROXY + item.link)
    htmlContent = response.data
  } catch (err) {
    __MYSTATION__.log('warning', 'MyFeeds error: unable to get HTML content from : "' + CORS_PROXY + item.link + '"', 'console')
    response = err
    htmlContent = null
  }
  if (response instanceof Error) {
    return {
      url: null,
      secure_url: null
    }
  }
  const virtualDOM = new DOMParser().parseFromString(htmlContent, 'text/html')
  const metaTags = [...virtualDOM.head.querySelectorAll('meta')]
  const imgURLProp = metaTags.find((tag) => { return tag.attributes.property && tag.attributes.property.value === 'og:image' })
  const imgSecureURLProp = metaTags.find((tag) => { return tag.attributes.property && tag.attributes.property.value === 'og:image:secure_url' })
  return {
    url: imgURLProp ? imgURLProp.attributes.content.value : null,
    secure_url: imgSecureURLProp ? imgSecureURLProp.attributes.content.value : null
  }
}

// Generate resource module mappers
const {
  feedsState,
  feedsGetters,
  feedsActions,
  feedsMutations
} = __MYSTATION__.generate.appModuleResourceMappers(__APP_NAME__, 'feeds')

const {
  categoriesState,
  categoriesGetters,
  categoriesActions,
  categoriesMutations
} = __MYSTATION__.generate.appModuleResourceMappers(__APP_NAME__, 'categories')

export default {
  namespaced: true,
  state: () => {
    return {
      // Generate:
      // feeds: [],
      // feedsLoading: false,
      // feedsCreating: false,
      // feedsUpdating: false,
      // feedsDeleting: false
      ...feedsState,
      // Same for categories
      ...categoriesState,
      // RSS Parser
      parser: new Parser(),
      corsProxy: CORS_PROXY,
      // Loaded indicator
      feedsLoaded: false // True when feeds have been loaded from server. False by default
    }
  },
  getters: {
    // Generate:
    // findFeeds()
    ...feedsGetters,
    // Same for categories
    ...categoriesGetters,
    /**
     * Retrieve feeds by category
     * @param {number} categoryId - The category id
     * @return {Object[]}
     */
    findByCategory: (state) => (categoryId) => {
      return state.feeds.filter((feed) => feed.category_id === categoryId)
    },
    /**
     * Return the last articles through any feed
     * @return {Object[]}
     */
    lastArticles: (state) => {
      let allArticles = []
      for (const i in state.feeds) {
        if (state.feeds[i].data) {
          const items = state.feeds[i].data.items
          allArticles = allArticles.concat(items)
        }
      }
      // Return sorted by Date
      return _.sortBy(allArticles, [function (a) { return new Date(a.pubDate) }]).reverse()
    },
    lastArticlesByCategory: (state, getters) => (categoryId) => {
      return getters.lastArticles.reduce((array, article) => {
        const feedCategory = getters.findFeeds({ id: article.feedId }).pop().category_id
        if (feedCategory === categoryId) {
          array.push(article)
        }
        return array
      }, [])
    },
    feedsDataLoading: (state) => {
      let toReturn = false
      state.feeds.forEach((feed) => {
        if (!feed.dataStatus) {
          toReturn = true
        }
      })
      return toReturn
    }
  },
  actions: {
    // Generate:
    // loadFeeds()
    // createFeeds()
    // updateFeeds()
    // deleteFeeds()
    ...feedsActions,
    // Same for categories
    ...categoriesActions,
    /**
     * Load feed data
     * @param {number} feedId - The feed id
     * @param {Object} options - Options for feed parser
     * @return {void}
     */
    loadFeedData: async function ({ state, commit, getters, dispatch }, feedId, options = {}) {
      // Loading display
      const toReturn = await __MYSTATION__.wait('', async () => {
        // Get data
        const feed = getters.findFeeds({ id: feedId }).pop()
        try {
          const data = await state.parser.parseURL(CORS_PROXY + feed.url)
          // Format data
          const tmpObject = data
          data.items.forEach((item, index) => {
            tmpObject.items[index] = {
              feedId: feedId,
              postImg: null,
              ...item
            }
          })
          // Commit
          commit('SET_FEED_DATA', {
            feedId: feedId,
            data: tmpObject
          })
          // Load async articles images
          dispatch('loadFeedArticlesImages', feedId)
          return true
        } catch (error) {
          return error
        }
      }, { loading: ['logoLoading'] })
      return toReturn
    },
    /**
     * loadFeedArticlesImages
     * Load article image
     * @param {number} feedId - The feed id
     * @return {void}
     */
    loadFeedArticlesImages: function ({ state, getters, commit }, feedId) {
      // Get data
      const feed = getters.findFeeds({ id: feedId }).pop()
      const tmpObject = feed.data
      feed.data.items.forEach(async (item, index) => {
        tmpObject.items[index] = {
          ...item,
          postImg: await findArticleImg(item)
        }
        // Commit
        commit('SET_FEED_DATA', {
          feedId: feedId,
          data: tmpObject
        })
      })
    },
    /**
     * Init feeds
     */
    initFeeds: async function ({ state, dispatch, commit }) {
      await dispatch('loadFeeds')
      await dispatch('loadCategories')
      commit('SET_FEEDS_LOADED', true)
      // Add object properties to each feed
      commit('SET_FEEDS', state.feeds.map(function (feed) {
        feed.data = null
        feed.dataStatus = false
        return feed
      }))
      // Then load source data with RSS Parser
      state.feeds.forEach((feed) => {
        dispatch('loadFeedData', feed.id)
      })
      return true
    }
  },
  mutations: {
    // Generate:
    // SET_FEEDS()
    // SET_FEEDS_LOADING()
    // SET_FEEDS_CREATING()
    // SET_FEEDS_UPDATING()
    // SET_FEEDS_DELETING()
    ...feedsMutations,
    // Same for categories
    ...categoriesMutations,
    /**
     * SET_FEED_DATA
     * Set data for a feed
     * @param {Object} payload
     * @param {number} payload.feedId
     * @param {Object[]} payload.data
     */
    SET_FEED_DATA (state, payload) {
      let feedIndex
      state.feeds.forEach((feed, index) => {
        if (feed.id === payload.feedId) {
          feedIndex = index
        }
      })
      const tmpObj = state.feeds[feedIndex]
      tmpObj.data = payload.data
      tmpObj.dataStatus = true
      // Trigger reactivity using Vue.set
      // because state.feeds is an Array, not an Object
      __VUE__.set(state.feeds, feedIndex, tmpObj)
    },
    /**
     * SET_FEEDS_LOADED
     * Set feedsLoaded
     * @param {boolean} value - True or false
     */
    SET_FEEDS_LOADED (state, value) {
      state.feedsLoaded = value
    }
  }
}
