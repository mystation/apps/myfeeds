/*    Init     */

const moduleName = __APP_NAME__ + '-' + 'MyFeedsModule'

// Map MyStation store module actions
/* eslint-disable no-unused-vars */
const createCategories = async (payload) => __MYSTATION__.$store.dispatch(moduleName + '/createCategories', payload)
const createFeeds = async (payload) => __MYSTATION__.$store.dispatch(moduleName + '/createFeeds', payload)
/* eslint-enable no-unused-vars */

export async function init () {
  // If app is not initialized for this user
  if (!__MYSTATION__.isAppInitialized(__APP_NAME__)) {
    // 'is_initialized' app status indicator
    // says to MyStation that the app needs a first user interaction.
    // It is particulary useful, for example:
    // - to inject default data after app installation
    // - define 'UI first step' tutorials, actions and behaviors...
    // Write code here. Do stuff for the app first use.
    // Warning! Not revertible status:
    // Once MyStation has saved app as 'initialized', it makes not possible to revert actions (ex: data injection).
    __MYSTATION__.log('warning', `App '${__APP_NAME__}' not initialized`, 'console')
    // Do stuff
    // ...
    // Display MyStation main loading
    await __MYSTATION__.wait(__MYSTATION__.$h.capitalize(__MYSTATION__.$i18n.t('global.loadingDescriptions.appInitialization', { app: __APP_NAME__ })), async function () {
      // // First, create feeds categorie
      // await createCategories({
      //   label: 'news',
      //   color: '#456869'
      // })
      // // Then, create feeds
      // await createFeeds({
      //   label: 'OpenClassrooms',
      //   url: 'https://blog.openclassrooms.com/feed/',
      //   category_id: 1
      // })
      // Don't forget to declare app as 'initialized'
      await __MYSTATION__.initializeApp(__APP_NAME__)
    })
  } else {
    __MYSTATION__.log('info', `App '${__APP_NAME__}' already initialized`, 'console')
  }
}

export default init
