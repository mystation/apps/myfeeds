module.exports = {
  root: true,
  globals: {
    // To disable eslint no-undef for this special variables computed by webpack
    // Check the file 'vue.config.js'
    GLOBALS: false, // because some lint errors in vue templates...
    __MYSTATION__: false,
    __MYSTATION_API__: false,
    __VUE__: false,
    __APP_NAME__: false,
    __APP_VERSION__: false,
    __APP_COLOR__: false,
    __T_PREFIX__: false
  },
  env: {
    node: true
  },
  extends: [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
  },
  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)'
      ],
      env: {
        jest: true
      }
    }
  ]
}
