![MyStation logo](https://gitlab.com/mystation/apps/myfeeds/-/raw/master/public/app.icon.png)

# MyStation app : myfeeds

[![mystation](https://img.shields.io/static/v1?label=MyStation&labelColor=191919&message=since%20v1.0.0&color=3D373A&logo=data:image/png;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAABILAAASCwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADTOpAA0zqQMGLK1AiblVxIe4VsAFKq85DDKqAg0zqQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANM6kADTOpAA0zqSUNM6mNCzGqx4e3VtqAsVvXCzCrxw0zqYMNM6keDTOpAA0zqQAAAAAAAAAAAA40qAAMM6kADTOpFg0zqXQNM6nKDTOpnAQpryy+9DGRvPIyggAlsjENM6miDTOpxQ0zqWcNM6kQDTOpAA81pgCSvQAFQmpiUxE4o8ANM6m0DTOpRg0zqQan2kEAv/UwkL/1MICs4D0ADTOpBw0zqUwNM6mzEjijtElxWUa24gADfqcUIHOdIs40XHVxAB7EDQ0zqQCo3D8Av/UwAL/1MJC/9TCAv/UwAA40qQANM6kDCi+uVyNJjOtxmiXYgqwOIX+pEiN/qRLEgKoRKICqEQAAAAAAv/UwAL/1MAC/9TCQv/UwgP//AAANM6ktDTOpmwwxq8wtVH+ZfacVzYCqESd/qRIjf6kSxH+pEih/qRIAAAAAAL/1MAC/9TAAwPYvj6XYQpcGK655DTOpzw0zqZkHLLIpfacVJ3+pEsV/qRInf6kSI3+pEsR/qRIof6kSALHlNQDB+DAAvPIyEsL4LquKu1T5HUWevQMosEAAG7gEcJklAH+pEid/qRLGf6kSJ3+pEiN/qRLEf6kSKIm1FwC78TEIvvQxT770ML+88S/Pue4y0rnuNMPG/CtJx/4oB4q1FQB/qRInf6kSxn+pEid/qRIjfqgSw4GrEyrB9zIzvvQwp7zxMNi26zCEqtwvHbbqKhq88i5+vvQv07/0MKPB9zExgasTKH6oEsV/qRInf6kSIou3F8y26imnvvQw17jtMaGu4jEwfqs3ApfHNACu4SgAn9AhAbnuLSy98i+ZwPYv0rbqKKOKthfOf6kSJ53MJR+r3jHamMtH6JnLRFTf/wgEvu4jAAAAAAAAAAAAAAAAAAAAAADR+BwA//8AA5vNRVGZy0jnq94w3JnIICLE+BMFZJFnWRpBn8UJL6yrDDKqOw40qAMNM6kAAAAAAAAAAAANM6kADjSoBAwyqkMJL6uxG0KewWqYYlPR/wAEARDOACJldgAMMascDTOpgw0zqc8NM6mNDTOpIg0zqQARN6YADTOpJw0zqZYNM6nPDTOpfAwwqxgYTpAAAA3NAAAAAAAAAAAADTOpAA0zqQINM6kyDTOpog0zqcdgjW52W4hxfA0zqcoNM6mcDTOpLQ0zqQENM6kAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADTOpAAowqwkPNadbjsBP24/ATdcPNaZUCS+tBw0zqQAAAAAAAAAAAAAAAAAAAAAA+B8AAOAPAADAAwAAAkAAAA5gAAAeQAAAHgAAABwIAAAQCAAAAAAAAAGAAAAH4AAAA8AAAMEDAADgBwAA+B8AAA==)](http://mystation.fr)
[![myfeeds](https://img.shields.io/static/v1?labelColor=F45B2B&label=myfeeds&message=v1.0.0&color=191919)]()
[![pipeline status](badges/master/pipeline.svg)](commits/master)

## Description

_MyFeeds_

__Version:__ `1.0.0`

__MyStation version:__ `1.0.0`

🌎 Translated
- en
- fr


## Dependencies

<details>
<summary>App dependencies</summary>

| name       | version |
| ---------- | ------- |
| rss-parser | ^3.8.0  |

</details>